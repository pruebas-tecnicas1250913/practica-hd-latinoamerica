<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebController@index')->name('home');

Route::post('/crear', 'WebController@api_crear')->name('api.crear');

Route::post('/activar', 'WebController@api_activar')->name('api.activar');//Activar
Route::post('/desactivar', 'WebController@api_desactivar')->name('api.desactivar');//Desactivar
Route::post('/renovar', 'WebController@api_renovar')->name('api.renovar');//Renovar
