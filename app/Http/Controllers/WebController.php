<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class WebController extends Controller{
    //Home
        public function index(){
            $consulta = DB::table('licencias')
                        ->select('*')
                        ->where('status', '<>',4)
                        ->get();

            foreach ($consulta as $key => $value) {
                $param['consulta'][$value->id]['id'] = $value->id;
                $param['consulta'][$value->id]['id_lic'] = $value->id_lic;
                $param['consulta'][$value->id]['codigo'] = $value->codigo;
                $param['consulta'][$value->id]['nombre'] = $value->nombre;

                if ($value->tiempo_termino != '') {
                    if ($value->tiempo_termino < time()) {
                        $param['consulta'][$value->id]['vigencia'] = 'Desactivada';
                    }elseif($value->tiempo_termino > time()){
                        $param['consulta'][$value->id]['vigencia'] = date('i', time());
                    }
                }else{
                    $param['consulta'][$value->id]['vigencia'] = $value->vigencia * 5;
                }

                $param['consulta'][$value->id]['tiempo_inicio'] = $value->tiempo_inicio;
                $param['consulta'][$value->id]['tiempo_termino'] = $value->tiempo_termino;


                $param['consulta'][$value->id]['producto'] = $value->producto *5;

                switch($value->status){
                    case '1':
                        $param['consulta'][$value->id]['status'] = 'Pendiente';
                        $param['consulta'][$value->id]['accion'] = '1';
                        break;
                    case '2':
                        $param['consulta'][$value->id]['status'] = 'Activado';
                        $param['consulta'][$value->id]['accion'] = '2';
                        break;

                    case '3':
                        $param['consulta'][$value->id]['status'] = 'Desactivada';
                        $param['consulta'][$value->id]['accion'] = '3';
                        break;
                    case '4':
                        $param['consulta'][$value->id]['status'] = 'Expirado';
                        $param['consulta'][$value->id]['accion'] = '4';
                        break;
                    default:
                        $param['consulta'][$value->id]['status'] = 'Expirado';
                        $param['consulta'][$value->id]['accion'] = '4';
                }

                $param['consulta'][$value->id]['created_at'] = $value->created_at;
                $param['consulta'][$value->id]['updated_at'] = $value->updated_at;
            }


            //Escuchamos a cual se desactiva
                $consulta_expired = DB::table('licencias')
                        ->select('*')
                        ->where('status', '<>',4)
                        ->where('tiempo_termino','<',time())
                        ->where('tiempo_termino','!=', '')
                        ->get();

                if (count($consulta_expired) > 0) {
                    $var_POST['id_lic'] = $consulta_expired[0]->id_lic;

                    $api = WebController::api('/disabled',$var_POST);
                    $api = json_decode($api);

                    if ($api->code == 201 || $api->code == 200) {
                        DB::table('licencias')
                        ->where('id_lic', $var_POST['id_lic'])
                        ->update([
                            'status' => $api->response->status,
                        ]);
                    }
                }
            //

            return view('welcome', $param);
        }
    //

    public function api_crear(Request $request){
        $var_POST = WebController::filtrar($request->input());

        //Validaciones
            //Nombre
                //Si no xiste nombre o esta vacio regresa mensajes de rror
                if (!isset($var_POST['name']) || $var_POST['name'] == '') {
                    $param['status'] = 'error';
                    $param['mensaje'] = 'Por favor, selecciona una vigencia disponible.';
                    $param['console'] = 'Error al seleccionar una vigencia: '.isset($var_POST['vigencia']);

                    $request->session()->flash('alert', $param);
                    return redirect()->back()->withInput();
                }
            //

            //Validacion numerica
                //Si no existe o esta vacio o no es numero regresa con mensajes de eror
                    //Vigencia
                    if (!isset($var_POST['vig']) || $var_POST['vig'] == '' || !is_numeric($var_POST['vig'])) {
                        $param['status'] = 'error';
                        $param['mensaje'] = 'Por favor, selecciona una vigencia disponible.';
                        $param['console'] = 'Error al seleccionar una vigencia: '.isset($var_POST['vig']);

                        $request->session()->flash('alert', $param);
                        return redirect()->back()->withInput();
                    }

                    //Producto
                    if (!isset($var_POST['prod']) || $var_POST['prod'] == '' || !is_numeric($var_POST['prod'])) {
                        $param['status'] = 'error';
                        $param['mensaje'] = 'Por favor, selecciona un producto disponible.';
                        $param['console'] = 'Error al seleccionar un producto: '.isset($var_POST['prod']);

                        $request->session()->flash('alert', $param);
                        return redirect()->back()->withInput();
                    }
                //

                //Si el valor es igual a 0 regresa con mensajes de error
                    //Vigencia
                    if ($var_POST['vig'] == 0) {
                        $param['status'] = 'error';
                        $param['mensaje'] = 'No puedes seleccionar una vigencia 0';
                        $param['console'] = 'Error al seleccionar una vigencia: '.isset($var_POST['vige']);

                        $request->session()->flash('alert', $param);
                        return redirect()->back()->withInput();
                    }

                    //Producto
                    if ($var_POST['prod'] == 0) {
                        $param['status'] = 'error';
                        $param['mensaje'] = 'No puedes seleccionar un producto 0';
                        $param['console'] = 'Error al seleccionar un producto: '.isset($var_POST['prod']);

                        $request->session()->flash('alert', $param);
                        return redirect()->back()->withInput();
                    }
                //
            //

        //Api
            $api = WebController::api('/create',$var_POST);
            $api = json_decode($api);

            if ($api->code == 201 || $api->code == 200) {
                if (isset($api->response->status)) {
                    DB::table('licencias')->insert([
                        'id_lic' => $api->response->id,
                        'codigo' => $api->response->code,
                        'nombre' => $api->response->name,
                        'vigencia' => $api->response->vig,
                        'tiempo_inicio' => '',
                        'tiempo_termino' => '',
                        'producto' => $api->response->prod,
                        'status' => $api->response->status,
                        'created_at' => date('Y-m-d H:i:s', $api->response->time_create),
                    ]);

                    $param['status'] = 'success';
                    $param['mensaje'] = 'Licencia creada corretamente';
                    $param['console'] = 'Sin problemas';

                    $request->session()->flash('alert', $param);
                    return redirect()->back();
                }else{
                    $param['status'] = 'error';
                    $param['mensaje'] = $api->mensaje;
                    $param['console'] = $api->mensaje;

                    $request->session()->flash('alert', $param);
                    return redirect()->back();
                }

            }else{
                $param['status'] = 'error';
                $param['mensaje'] = $api->mensaje.': '. isset($api->param[0]).' '.isset($api->param[1]).' '.isset($api->param[1]);
                $param['console'] = 'Estan vacios'.isset($api->param);

                $request->session()->flash('alert', $param);
                return redirect()->back();
            }
        //
    }

    public function filtrar($input){
        foreach ($input as $key => $value){
            $new_1 = str_replace('*', '', $value);
            $new_2 = str_replace('/', '', $new_1);
            $new_3 = str_replace('?', '', $new_2);
            $new_4 = str_replace('¿', '', $new_3);
            $new_5 = str_replace('"', '', $new_4);
            $new_6 = str_replace("'", '', $new_5);
            $new_7 = str_replace('(', '', $new_6);
            $new_8 = str_replace(')', '', $new_7);
            $new_9 = str_replace('$', '', $new_8);
            $new_10 = str_replace('&', '', $new_9);
            $new_11 = str_replace('!', '', $new_10);
            $new_12 = str_replace('¡', '', $new_11);
            $new_13 = str_replace('[', '', $new_12);
            $new_14 = str_replace(']', '', $new_13);
            $new_15 = str_replace('{', '', $new_14);
            $new_16 = str_replace('}', '', $new_15);
            $new_17 = str_replace('~', '', $new_16);
            $new_18 = str_replace('^', '', $new_17);
            $new_19 = str_replace('<', '', $new_18);
            $new_20 = str_replace('\\', '', $new_19);
            $new_21 = str_replace('+', '', $new_20);
            $new_22 = str_replace('-', '', $new_21);
            $new_23 = str_replace("`", "", $new_22);
            $new_24 = str_replace(";", "", $new_23);
            $new_25 = str_replace("&", "", $new_24);

            $return[$key] = $new_25;
        }

        return $return;
    }

    public function api_activar(Request $request){
        $var_POST = WebController::filtrar($request->input());

        //Validamos campo
        if(!isset($var_POST['id_lic']) || !is_numeric($var_POST['id_lic'])){
            $param['status'] = 'error';
            $param['mensaje'] = 'Por favor selecciona una licencia disponible';
            $param['console'] = 'Licencia erronea: '.isset($var_POST['id_lic']);

            $request->session()->flash('alert', $param);
            return redirect()->back()->withInput();
        }

        //Validamos existencia
            $exis = DB::table('licencias')
                    ->select('*')
                    ->where('id_lic', $var_POST['id_lic'])
                    ->get();

            if (count($exis) <= 0) {
                $param['status'] = 'error';
                $param['mensaje'] = 'Por favor selecciona una licencia disponible';
                $param['console'] = 'Licencia erronea: '.isset($var_POST['id_lic']);

                $request->session()->flash('alert', $param);
                return redirect()->back()->withInput();
            }
        //

        //Api
            $api = WebController::api('/activate',$var_POST);
            $api = json_decode($api);
            if ($api->code == 201 || $api->code == 200) {
                if (isset($api->response->status)) {
                    DB::table('licencias')
                    ->where('id_lic', $var_POST['id_lic'])
                    ->update([
                        'status' => $api->response->status,
                        'tiempo_inicio' => $api->response->time_active,
                        'tiempo_termino' => $api->response->time_vig,
                    ]);

                    $param['status'] = 'success';
                    $param['mensaje'] = $api->mensaje;
                    $param['console'] = 'Sin problemas';

                    $request->session()->flash('alert', $param);
                    return redirect()->back();
                }else{
                    $param['status'] = 'error';
                    $param['mensaje'] = $api->mensaje;
                    $param['console'] = $api->mensaje;

                    $request->session()->flash('alert', $param);
                    return redirect()->back();
                }

            }else{
                $param['status'] = 'error';
                $param['mensaje'] = $api->mensaje.': '. isset($api->param[0]).' '.isset($api->param[1]).' '.isset($api->param[1]);
                $param['console'] = $api->mensaje;

                $request->session()->flash('alert', $param);
                return redirect()->back();
            }
        //
    }

    public function api_desactivar(Request $request){
        $var_POST = WebController::filtrar($request->input());

        //Validamos campo
        if(!isset($var_POST['id_lic']) || !is_numeric($var_POST['id_lic'])){
            $param['status'] = 'error';
            $param['mensaje'] = 'Por favor selecciona una licencia disponible';
            $param['console'] = 'Licencia erronea: '.isset($var_POST['id_lic']);

            $request->session()->flash('alert', $param);
            return redirect()->back()->withInput();
        }

        //Validamos existencia
            $exis = DB::table('licencias')
                    ->select('*')
                    ->where('id_lic', $var_POST['id_lic'])
                    ->get();

            if (count($exis) <= 0) {
                $param['status'] = 'error';
                $param['mensaje'] = 'Por favor selecciona una licencia disponible';
                $param['console'] = 'Licencia erronea: '.isset($var_POST['id_lic']);

                $request->session()->flash('alert', $param);
                return redirect()->back()->withInput();
            }
        //

        //Api
            $api = WebController::api('/disabled',$var_POST);
            $api = json_decode($api);

            if ($api->code == 201 || $api->code == 200) {
                if (isset($api->response->status)) {
                    DB::table('licencias')
                    ->where('id_lic', $var_POST['id_lic'])
                    ->update([
                        'status' => $api->response->status,
                        'tiempo_inicio' => '',
                        'tiempo_termino' => '',
                    ]);

                    $param['status'] = 'success';
                    $param['mensaje'] = $api->mensaje;
                    $param['console'] = 'Sin problemas';

                    $request->session()->flash('alert', $param);
                    return redirect()->back();
                } else {
                    $param['status'] = 'error';
                    $param['mensaje'] = $api->mensaje;
                    $param['console'] = 'Sin problemas';

                    $request->session()->flash('alert', $param);
                    return redirect()->back();
                }
            }else{
                $param['status'] = 'error';
                $param['mensaje'] = $api->mensaje.': '. isset($api->param[0]).' '.isset($api->param[1]).' '.isset($api->param[1]);
                $param['console'] = $api->mensaje;

                $request->session()->flash('alert', $param);
                return redirect()->back();
            }
        //
    }

    public function api_renovar(Request $request){
        $var_POST = WebController::filtrar($request->input());

        //Validamos campo
        if(!isset($var_POST['id_lic']) || !is_numeric($var_POST['id_lic'])){
            $param['status'] = 'error';
            $param['mensaje'] = 'Por favor selecciona una licencia disponible';
            $param['console'] = 'Licencia erronea: '.isset($var_POST['id_lic']);

            $request->session()->flash('alert', $param);
            return redirect()->back()->withInput();
        }

        //Validamos existencia
            $exis = DB::table('licencias')
                    ->select('*')
                    ->where('id_lic', $var_POST['id_lic'])
                    ->get();

            if (count($exis) <= 0) {
                $param['status'] = 'error';
                $param['mensaje'] = 'Por favor selecciona una licencia disponible';
                $param['console'] = 'Licencia erronea: '.isset($var_POST['id_lic']);

                $request->session()->flash('alert', $param);
                return redirect()->back()->withInput();
            }
        //

        //Api
            $var_POST['vig'] = $exis[0]->vigencia;



            $api = WebController::api('/renew',$var_POST);
            $api = json_decode($api);

            if ($api->code == 201 || $api->code == 200) {

                if (isset($api->response->status)) {
                    DB::table('licencias')
                    ->where('id_lic', $var_POST['id_lic'])
                    ->update([
                        'status' => $api->response->status,
                        'tiempo_inicio' => $api->response->time_active,
                        'tiempo_termino' => $api->response->time_vig,
                    ]);

                    $param['status'] = 'success';
                    $param['mensaje'] = $api->mensaje;
                    $param['console'] = 'Sin problemas';

                    $request->session()->flash('alert', $param);
                    return redirect()->back();
                }else{

                    $param['status'] = 'error';
                    $param['mensaje'] = $api->mensaje;
                    $param['console'] = $api->mensaje;

                    $request->session()->flash('alert', $param);
                    return redirect()->back();
                }

            }else{
                $param['status'] = 'error';
                $param['mensaje'] = $api->mensaje.': '.$api->param[0];
                $param['console'] = $api->mensaje.': '.$api->param[0];

                $request->session()->flash('alert', $param);
                return redirect()->back();
            }
        //
    }

    /* API PRACTICA */
        private function api($url, $form){

            if ($url == '') {
                $url = '/';
            }

            if ($form == '' || $form == NULL || $form == false) {
                $form = '';
            }

            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://practica.hdlatinoamerica.com/api'.$url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $form,
            CURLOPT_HTTPHEADER => array(

                'token: '.env('api_token', 'token_mOJ8GK5qUNyHamptgEWnTPTOxXXMCYCCKK5yBrTDDfzNshr9bpka7AFbjVJPPOLF5UZBbL5MtN4hqKJxvG9LdfzaMh'),
                'Authorization: Basic '.base64_encode(env('api_user', 'user_393623830607594').':'.env('api_password', 'Hi9hEs3KxUM6qMzeSVEIRXDpG6frrr'))
            ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            return $response;
        }
    /*  */

}
